from django.db import models
from django.conf import settings
import datetime

# Create your models here.
class setoran_sampah(models.Model):
    tanggal = models.DateField(default=datetime.date.today)
    penyetor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    jenis_sampah = (
        ('Plastik', 'Plastik'),
        ('Ember', 'Ember'),
        ('Kardus', 'Kardus'),
    )
    jenis = models.CharField(
        max_length=20,
        choices=jenis_sampah,
    )

    jumlah_setoran = models.IntegerField()
    total_poin = models.IntegerField()

    pilihan_status = (
        ('Approved', 'Approved'),
        ('Menunggu Validasi', 'Menunggu Validasi'),
    )
    status = models.CharField(
        max_length=20,
        choices=pilihan_status,
    )

    pendapatan = models.IntegerField()

    class Meta:
        verbose_name_plural = "setoran_sampah"

    def __str__(self):
        return self.penyetor.username
