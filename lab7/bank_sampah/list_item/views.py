from django.shortcuts import render, redirect
from . import models
from . import forms
from django.contrib.auth.models import User

# Create your views here.
def home(request):
    semua_sampah = models.setoran_sampah.objects.all()
    print(len(semua_sampah))

    context = {
        'semua_sampah': semua_sampah,
    }

    return render(request, 'home.html', context)

def create(request):
    form = forms.SampahForm
    all_user = User.objects.all()

    if request.method == 'POST':
        print(request.POST)
        form = forms.SampahForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {
        'form': form,
        'users': all_user,
    }

    return render(request, 'create.html', context)

def edit(request, pk):
    pilihan_sampah = models.setoran_sampah.objects.get(id=pk)
    all_user = User.objects.all()

    if request.method == 'POST':
        print(request.POST)
        form = forms.SampahForm(request.POST, instance=pilihan_sampah)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {
        'pk': pk,
        'pilihan_sampah': pilihan_sampah,
        'users': all_user,
    }

    return render(request, 'edit.html', context)

def delete(request, pk):
    pilihan_sampah = models.setoran_sampah.objects.get(id=pk)

    if request.method == 'POST':
        pilihan_sampah.delete()
        return redirect('/')

    context = {
        'pilihan_sampah': pilihan_sampah
    }

    return render(request, 'delete.html', context)