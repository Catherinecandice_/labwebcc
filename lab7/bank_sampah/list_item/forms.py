from django.db.models.base import Model
from django.forms import ModelForm, fields
from . import models

class SampahForm(ModelForm):
    class Meta:
        model = models.setoran_sampah
        fields = '__all__'