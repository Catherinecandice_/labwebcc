from django.shortcuts import render
from . import models

# Create your views here.

def home(request):
    daftar_kopi = models.kopi.objects.all()
    home_kopi = models.homepageproducts.objects.all()

    context = {
        'daftar_kopi': daftar_kopi,
        'kopi_1': daftar_kopi[0],
        'home_kopi': home_kopi,
    }

    return render(request, 'home.html', context)

def about(request):

    context = {
        'navbar_color': 'black',
    }

    return render(request, 'about.html', context)

def products(request):
    daftar_kopi = models.kopi.objects.all()

    context = {
        'daftar_kopi': daftar_kopi,
        'navbar_color': 'black',
    }

    return render(request, 'products.html', context)

def productdetails(request, pk):
    kopi_details = models.kopi.objects.get(id=pk)

    context = {
        'kopi_details': kopi_details,
        'navbar_color': 'black',
    }

    return(request, 'productdetails.html', context)