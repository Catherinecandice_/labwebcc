from django.db import models

# Create your models here.
class kopi(models.Model):
    nama = models.CharField(max_length=50)
    gambar = models.ImageField(upload_to="images")
    description = models.CharField(max_length = 200, default="")
    stok = models.IntegerField(default=0)
    tersedia = models.BooleanField(default=True)
    pilihan_merek = [
        ('Nescafe', 'Nescafe'),
        ('Kapal Api', 'Kapal Api'),
        ('Indocafe', 'Indocafe'),
        ('Torabika', 'Torabika'),
    ]
    merek = models.CharField(
        max_length=20,
        choices=pilihan_merek,
    )

    def __str__(self):
        return self.nama

class homepageproducts(models.Model):
    title = models.CharField(max_length = 50)
    description = models.CharField(max_length = 100)
    image = models.ImageField(upload_to="homeimages")

    def __str__(self):
        return self.title