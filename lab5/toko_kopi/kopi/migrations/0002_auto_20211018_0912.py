# Generated by Django 3.2.7 on 2021-10-18 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kopi', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='homepageproducts',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='homeimages')),
            ],
        ),
        migrations.AddField(
            model_name='kopi',
            name='description',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='kopi',
            name='merek',
            field=models.CharField(choices=[('Nescafe', 'Nescafe'), ('Kapal Api', 'Kapal Api'), ('Indocafe', 'Indocafe'), ('Torabika', 'Torabika')], max_length=20),
        ),
    ]
