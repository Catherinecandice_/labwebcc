from django.db.models import fields
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from . import models

# Create your views here.
class homeView(ListView):
    model = models.kaos
    template_name = 'home.html'

class kaosView(DetailView):
    model = models.kaos
    template_name = 'detail.html'

class addKaos(CreateView):
    model = models.kaos
    template_name = 'create.html'

    fields = '__all__'

class updateKaos(UpdateView):
    model = models.kaos
    template_name = 'update.html'

    fields = '__all__'

class deleteKaos(DeleteView):
    model = models.kaos
    template_name = 'delete.html'
    success_url = reverse_lazy('home')
