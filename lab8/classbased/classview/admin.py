from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.kategori_kaos)
admin.site.register(models.bahan_kaos)
admin.site.register(models.kaos)
