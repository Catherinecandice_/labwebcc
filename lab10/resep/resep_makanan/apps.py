from django.apps import AppConfig


class ResepMakananConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'resep_makanan'
