import React from "react";
import ResepList from "../components/resep-list";
import Nav from "../components/nav";

function resep() {
    return (
        <div>
            <Nav />
            <ResepList />
        </div>
    );
}

export default resep;