import './App.css';
import React from 'react'
import { Routes, Route, Link } from "react-router-dom";
import Resep from './pages/resep';
import CreateResep from './pages/create-resep';
// import ResepList from './components/resep-list';
// import Nav from './components/nav';

function App() {
  return (
    <div>
      <Routes>
        <Route path="/resep" element={<Resep />} />
        <Route path="/create/resep" element={<CreateResep />} />
      </Routes>
    </div>
  );
}

// function App() {
//   return (
//     <div>
//       <Nav />
//       <ResepList />
//     </div>
//   );
// }

export default App;
