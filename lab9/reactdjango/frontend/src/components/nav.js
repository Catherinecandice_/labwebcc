import './styles.css'
import { Link } from 'react-router-dom';

function nav(){

    return(
        <div>
            <ul className="nav-selection">
                <Link to="/resep"><li>Home</li></Link>
                <li>About</li>
                <Link to="/create/resep"><li>Create</li></Link>
            </ul>
        </div>
    )
}

export default nav;