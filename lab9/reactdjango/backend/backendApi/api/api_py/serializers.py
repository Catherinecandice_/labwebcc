from rest_framework import serializers
from .. import models

class ApiModelSerializer(serializers.ModelSerializer):
    tipe = serializers.SerializerMethodField()

    class Meta:
        model = models.resepMakanan
        fields = '__all__'

    def get_tipe(self, obj):
        print(self)
        print(obj)
        return obj.tipe.judul

class ApiModelSerializer2(serializers.ModelSerializer):
    class Meta:
        model = models.tipeMakanan
        fields = '__all__'