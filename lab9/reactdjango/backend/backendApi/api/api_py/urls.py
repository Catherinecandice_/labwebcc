from django.urls import path
from . import views

urlpatterns = [
    path('resepMakanan/', views.api_list),
    path('tipeMakanan/', views.api_list2)
]