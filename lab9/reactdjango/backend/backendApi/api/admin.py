from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.tipeMakanan)
admin.site.register(models.resepMakanan)
admin.site.register(models.bahanMakanan)