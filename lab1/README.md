*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Roboto';
}

.base{
    width: 100vw;
    height:100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    background-image: linear-gradient(to bottom right, #1C3ADF, #200C5B);
}

.box{
    width: 500px;
    background-color: white;
    border-radius: 10px;
}

.contents{
    margin: 40px;
}

.title{
    margin-bottom: 4px;
}

.title-description{
    font-size: 12px;
    color: grey;
    margin-bottom: 20px;
}

.inputs{
    font-weight: bold;
}

.input-box{
    border-width: 0px;
    background-color: #D5D5F5;
    width: 100%;
    height: 50px;
    border-radius: 5px;
    margin-bottom: 40px;
    padding: 20px;
}

.login-button{
    background-color: #4844CC;
    padding: 15px 20px;
    text-align: center;
    color: white;
    font-weight: bold;
    border-radius: 5px;
    margin-bottom: 10px;
}

.no-account{
    text-align: center;
    font-size: 14px;
    font-weight: bold;
    color: grey;
}

.create-now{
    color: red;
}

.password-box{
    display: flex;
    justify-content: space-between;
}

.lupa-password{
    color: #0645AD;
}